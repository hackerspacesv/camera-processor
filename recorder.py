import daemon
import subprocess
import signal
import sys
import time
import threading
from processes_config import processes

do_polling = True
timer = None

def check_process():
    global timer
    global do_polling
    if do_polling:
        for p in processes:
            if p['instance'].poll() is not None:
                print("Re-launching process {}...".format(p['name']))
                p['instance'] = subprocess.Popen(p['args'])
    else:
        for p in processes:
            if p['instance'].poll() is None:
                p['instance'].kill()

    if do_polling:
        print("Sleeping for 10 seconds. Before polling again.")
        timer = threading.Timer(10.0, check_process )
        timer.start()

def signal_handler(sig, frame):
    print("Terminating process")
    global do_polling
    do_polling = False
    timer.cancel()

signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGINT, signal_handler)

with daemon.DaemonContext():
    print("Launching process...")
    
    timer = threading.Timer(10.0, check_process )

    for p in processes:
        p['instance'] = subprocess.Popen(p['args'])
        print("{} started!".format(p['name']))

    timer.start()

    print("All processes ended. Closed daemon.")

